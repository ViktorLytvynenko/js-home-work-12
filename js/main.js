/// Theory
// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?
// Из-за устройств таких как мобильные телефоны, планшеты и так далее.

/// Practice

let buttons = document.querySelectorAll(".btn");
let lastButton = null;
document.addEventListener("keydown", (event) => {
    if (lastButton !== null && lastButton.classList.contains("active")) {
        lastButton.classList.remove("active");
        lastButton.classList.add("inactive");
    }
    for (let btn of buttons) {
        if (event.key.toUpperCase() === btn.innerText.toUpperCase()) {
            if (btn.classList.contains("inactive")) {
                btn.classList.remove("inactive");
                btn.classList.add("active");
            }
            lastButton = btn;
        }
    }
});